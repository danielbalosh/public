#!/bin/bash
source layers/bin/activate
pip install -r in/requirements.txt -t ./packages
deactivate
zip -r packages.zip ./packages/
cp packages.zip ./out/
zip -r nltk_data.zip ./nltk_data/
cp nltk_data.zip ./out/
