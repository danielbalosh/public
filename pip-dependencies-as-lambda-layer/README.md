# Example for installing pip dependencies as Lambda layer, including nltk_data

# Run

*   create two zips
   
    ```bash
    docker build . -t lc
    docker run  -v $(pwd)/in:/app/in/ -v $(pwd)/out/:/app/out/ lc:latest /bin/bash build.sh
    ```
*   create lambda layer from each

*   use lambda layer at lambda:
    * select layers at design editor
    * set Environment variables: ```NLTK_DATA = /opt/nltk_data```
    * add layers to python path:
    
    ```python
    import sys
    sys.path.append("/opt/packages/")
    sys.path.append("/opt/nltk_data/")
    ```

# Credits
*   https://forums.aws.amazon.com/message.jspa?messageID=846582#847156
*   https://medium.com/@jenoyamma/how-to-install-python-packages-for-aws-lambda-layer-74e193c76a91
